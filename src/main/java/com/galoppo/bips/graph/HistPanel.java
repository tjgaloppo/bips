/*
 * Copyright © 2016, Travis Galoppo <tjg2107@columbia.edu>
 *
 * Permission to use, copy, modify, and/or distribute this software for any 
 * purpose with or without fee is hereby granted, provided that the above 
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES 
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY 
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF 
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN 
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
package com.galoppo.bips.graph;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.Arrays;
import javax.swing.JPanel;

/**
 *
 * @author tgaloppo
 */
public class HistPanel extends JPanel {
    private final int[] binCounts;
    private final double[] binBounds;
    private final String label;
    private final int maxHeight;
    
    public HistPanel(int[] binCounts, double[] binBounds, String label) {
        super();
        setPreferredSize(new Dimension(600, 400));
        this.binCounts = binCounts;
        this.binBounds = binBounds;
        this.label = label;
        maxHeight = Arrays.stream(binCounts).reduce(Integer::max).getAsInt();
    }
    
    @Override
    public void paint(Graphics g) {
        int width = this.getWidth();
        int height = this.getHeight();
        
        // clear the view area
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, width, height);
        
        // draw the axes
        g.setColor(Color.BLACK);
        g.drawLine(10, height-20, width-10, height-20);
        g.drawLine(10, 20, 10, height-20);
        
        // draw the bars
        int drawWidth = width - 30;
        int drawHeight = height - 40;
        int barWidth = drawWidth / binCounts.length;
        for (int j=0; j<binCounts.length; j++) {
            int barHeight = drawHeight * binCounts[j] / maxHeight;
            g.drawRect(15+j*barWidth, height-barHeight-20, barWidth, barHeight);
        }
        
        // draw the x axis labels
        for (int j=0; j<binBounds.length; j++) {
            String str = String.format("%.2f", binBounds[j]);
            int offset = (int)(g.getFontMetrics().getStringBounds(str, g).getWidth() / 2.0);
            int x = 15 + j * barWidth;
            g.drawLine(x, height-20, x, height-16);
            g.drawString(str, 15+j*barWidth-offset, height-6);
        }
        
        // draw the variable label
        g.drawString(label, 10, 10);
    }
}
