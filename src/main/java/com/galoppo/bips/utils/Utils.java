/*
 * Copyright © 2016, Travis Galoppo <tjg2107@columbia.edu>
 *
 * Permission to use, copy, modify, and/or distribute this software for any 
 * purpose with or without fee is hereby granted, provided that the above 
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES 
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY 
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF 
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN 
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
package com.galoppo.bips.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author tgaloppo
 */
public class Utils {
    public static double[][] readData(File dataFile, boolean hasHeader) throws Exception {
        BufferedReader r = new BufferedReader(new FileReader(dataFile));
        List<double[]> list = new LinkedList<>();
        String inp = null;
        if (hasHeader)
            r.readLine();
        while (null != (inp = r.readLine())) {
            String[] parts = inp.split(",");
            double[] row = new double[parts.length];
            for (int j=0; j<row.length; j++)
                row[j] = Double.parseDouble(parts[j]);
            list.add(row);
        }
        return list.toArray(new double[0][]);
    }

    public static double[] getColumnValues(double[][] data, int col) {
        double[] vals = new double[data.length];
        for (int j=0; j<vals.length; j++)
            vals[j] = data[j][col];
        return vals;
    }        

    /**
     * Returns the n-th largest value in an array of double values.  If
     * the array were sorted in ascending order, this would be x[n]. NOTE
     * that the array does not need to be sorted and will not be sorted
     * by this function.
     * 
     * @param x array of double values
     * @param n zero based order statistic sought
     * @return the n-th largest value in x.
     */
    public static double nth(double[] x, int n)
    {
        List<Double> data = new ArrayList<>();
        for(double w : x) data.add(w);
        int left=0, right=x.length-1;
        while(left != right)
        {
            double pivot = data.get(data.size()/2);
            List<Double> more = new ArrayList<>();
            List<Double> less = new ArrayList<>();
            for(Double w : data)
            {
                if(w < pivot) 
                    less.add(w);
                else if(w > pivot) 
                    more.add(w);
            }

            if(n < left + less.size())
            {
                right = left + less.size() - 1;
                data = less;
            } 
            else if(n > right - more.size())
            {
                left = right - more.size() + 1;
                data = more;
            }
            else
            {
                return pivot;
            }
        }

        return data.get(0);
    }    
}
