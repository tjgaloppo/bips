/*
 * Copyright © 2016, Travis Galoppo <tjg2107@columbia.edu>
 *
 * Permission to use, copy, modify, and/or distribute this software for any 
 * purpose with or without fee is hereby granted, provided that the above 
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES 
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY 
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF 
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN 
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
package com.galoppo.bips.utils;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * A wildly inefficient summary statistic utility class
 * 
 * @author tgaloppo
 */
public class Summary {
    private String[] label;
    private double[] min, max;
    private double[] mean, std;
    private double[] median, q1, q3;
    
    public Summary(List<double[]> data, String[] labels){
        init(data.toArray(new double[0][]), labels);
    }
    public Summary(double[][] data, String[] labels) {
        init(data, labels);
    }

    public Summary(double[][] data) {
        this(data, Stream.iterate(0, (n)->n+1)
                         .limit(data[0].length)
                         .map(j -> String.format("var_%d", j))
                         .collect(Collectors.toList())
                         .toArray(new String[0]));
    }
    
    public Summary(List<double[]> data) {
        this(data.toArray(new double[0][]));
    }
    
    private void init(double[][] data, String[] labels){
        min    = computeMins(data);
        max    = computeMaxs(data);
        mean   = computeMeans(data);
        std    = computeStandardDeviations(data);
        median = computeMedians(data);
        q1     = computeQuartile1(data);
        q3     = computeQuartile3(data);
        label  = labels;        
    }

    @Override
    public String toString() {
        StringBuilder b = new StringBuilder();
        
        int nrows = (label.length + 2) / 3;
        for (int j=0; j<nrows; j++) {
            int first = j * 3;
            int stop  = Math.min((j+1)*3, label.length);
            for (int k=first; k<stop; k++) {
                b.append(String.format("%-23s", label[k]));
            }
            b.append("\n");
            for (int k=first; k<stop; k++) {
                b.append(String.format("%-23s", String.format("       min: %4.3f", min[k])));
            }
            b.append("\n");        
            for (int k=first; k<stop; k++) {
                b.append(String.format("%-23s", String.format("       max: %4.3f", max[k])));
            }
            b.append("\n");        
            for (int k=first; k<stop; k++) {
                b.append(String.format("%-23s", String.format("      mean: %4.3f", mean[k])));
            }
            b.append("\n");
            for (int k=first; k<stop; k++) {
                b.append(String.format("%-23s", String.format("       std: %4.3f", std[k])));
            }
            b.append("\n");
            for (int k=first; k<stop; k++) {
                b.append(String.format("%-23s", String.format("    median: %4.3f", median[k])));
            }
            b.append("\n");
            for (int k=first; k<stop; k++) {
                b.append(String.format("%-23s", String.format("quartile 1: %4.3f   ", q1[k])));
            }
            b.append("\n");
            for (int k=first; k<stop; k++) {
                b.append(String.format("%-23s", String.format("quartile 3: %4.3f", q3[k])));
            }
            b.append("\n\n");              
        }

        return b.toString();
    }

    private double[] computeMins(double[][] data) {
        int k = data[0].length;
        double[] mins = new double[k];      
        for (int i=0; i<k; i++) {
            final int col = i;
            mins[i] = Stream.iterate(0, (n)->n+1)
                             .limit(data.length)
                             .map(j -> data[j][col])
                             .reduce(Double.POSITIVE_INFINITY, (a,b) -> a<b?a:b);
                             
        }
        return mins;
    }    
   
    private double[] computeMaxs(double[][] data) {
        int k = data[0].length;
        double[] maxs = new double[k];      
        for (int i=0; i<k; i++) {
            final int col = i;
            maxs[i] = Stream.iterate(0, (n)->n+1)
                             .limit(data.length)
                             .map(j -> data[j][col])
                             .reduce(Double.NEGATIVE_INFINITY, (a,b) -> a>b?a:b);
                             
        }
        return maxs;
    }        
    
    private double[] computeMeans(double[][] data) {
        int k = data[0].length;
        double[] means = new double[k];      
        for (int i=0; i<k; i++) {
            final int col = i;
            means[i] = Stream.iterate(0, (n)->n+1)
                             .limit(data.length)
                             .collect(Collectors.averagingDouble(j -> data[j][col]));
                             
        }
        return means;
    }
    
    private double[] computeStandardDeviations(double[][] data) {
        int k = data[0].length;
        double[] stds = new double[k];
        for (int i=0; i<k; i++) {
            final int col = i;
            final double mu = mean[i];
            double mean_sq = Stream.iterate(0, (n)->n+1)
                                   .limit(data.length)
                                   .map(j -> data[j][col] - mu)
                                   .collect(Collectors.averagingDouble(j -> j*j));
            stds[i] = Math.sqrt(mean_sq);
        }
        return stds;
    } 

    private double[] computeMedians(double[][] data) {
        int k = data[0].length;
        int n = data.length;
        double[] medians = new double[data[0].length];
        for (int i=0; i<k; i++) {
            double[] colVals = Utils.getColumnValues(data, i);
            medians[i] = Utils.nth(colVals, 50 * n / 100);
        }  
        return medians;
    }
    
    private double[] computeQuartile1(double[][] data) {
        int k = data[0].length;
        int n = data.length;
        double[] q1s = new double[data[0].length];
        for (int i=0; i<k; i++) {
            double[] colVals = Utils.getColumnValues(data, i);
            q1s[i] = Utils.nth(colVals, 25 * n / 100);
        }  
        return q1s;
    } 
    
    private double[] computeQuartile3(double[][] data) {
        int k = data[0].length;
        int n = data.length;
        double[] q3s = new double[data[0].length];
        for (int i=0; i<k; i++) {
            double[] colVals = Utils.getColumnValues(data, i);
            q3s[i] = Utils.nth(colVals, 75 * n / 100);
        }  
        return q3s;
    }    
}
