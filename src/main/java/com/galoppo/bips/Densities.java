/*
 * Copyright © 2016, Travis Galoppo <tjg2107@columbia.edu>
 *
 * Permission to use, copy, modify, and/or distribute this software for any 
 * purpose with or without fee is hereby granted, provided that the above 
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES 
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY 
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF 
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN 
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
package com.galoppo.bips;

import java.util.Arrays;

/**
 * 
 * @author tgaloppo
 */
public class Densities {
    // log(sqrt(2*pi))
    private static final double logRoot2pi = Math.log(2.0 * Math.PI) / 2.0;
    // Euler-Mascheroni constant
    private static final double emc = 0.57721566490153286060651209; 
        
    /**
     * Density function for uniform distribution
     * 
     * @param x Point at which density is to be computed
     * @param a Lower bound of uniform distribution
     * @param b Upper bound of uniform distribution
     * @return Log-density of U(a,b) at point x
     */
    public static double uniform(double x, double a, double b) {
        if (x >= a && x <= b)
            return -Math.log(b-a);
        else
            return Double.NEGATIVE_INFINITY;
    }
    
    /**
     * Density function for uniform distribution
     * 
     * @param x Array of points at which density is to be computed
     * @param a Lower bound of uniform distribution
     * @param b Upper bound of uniform distribution
     * @return Array of log-density of U(a,b) at point x[0], x[1], ...
     */    
    public static double[] uniform(double[] x, double a, double b) {
        double log_delta = -Math.log(b-a);
        return Arrays.stream(x)
                     .map(u -> (u >= a && u <= b) ? log_delta : Double.NEGATIVE_INFINITY)
                     .toArray();
    }
    
    /**
     * Density function for Gaussian (Normal) distribution
     * 
     * @param x Point at which density is to be computed
     * @param mu Mean of the normal distribution
     * @param sigma Standard deviation of the normal distribution
     * @return Log-density of N(mu,sigma) at point x
     */
    public static double gaussian(double x, double mu, double sigma) {
        double delta = (x - mu);
        double ratio = delta / sigma;
        return -logRoot2pi - Math.log(sigma) - 0.5 * ratio * ratio;
    }
    
    /**
     * Density function for Gaussian (Normal) distribution
     * 
     * @param x Array of points at which density is to be computed
     * @param mu Mean of the normal distribution
     * @param sigma Standard deviation of the normal distribution
     * @return Array of log-densities of N(mu,sigma) at points x[0], x[1], ...
     */
    public static double[] gaussian(double[] x, double mu, double sigma) {
        double[] v = new double[x.length];
        double u = -logRoot2pi - Math.log(sigma);
        for (int j=0; j<v.length; j++) {
            double delta = x[j] - mu;
            double ratio = delta / sigma;
            v[j] = u - 0.5 * ratio * ratio;
        }
        return v;
    }
        
    /**
     * Density function for Chi-Squared distribution
     * 
     * @param x Point at which density is to be evaluated
     * @param k Degrees of freedom
     * @return log-density of ChiSquared(k) at x
     */
    public static double chisquared(double x, double k) {
        double k_2 = k / 2.0;
        return (k_2 - 1) * Math.log(x) - x / 2.0 - k_2 * Math.log(2.0) - lngamma(k_2);
    }
    
    /**
     * Density function for Chi-Squared distribution
     * 
     * @param x Array of points at which density is to be evaluated
     * @param k Degrees of freedom
     * @return log-density of ChiSquared(k) at x[0], x[1], ...
     */    
    public static double[] chisquared(double[] x, double k) {
        double k_2 = k / 2.0;
        double u = k_2 * Math.log(2.0) + lngamma(k_2);
        return Arrays.stream(x).map(a -> (k_2-1)*Math.log(a)-a/2.0-u).toArray();
    }
    
    /**
     * Density function for Exponential distribution
     * 
     * @param x Point at which density is to be computed
     * @param lambda Mean of the exponential distribution
     * @return Log-density of exp(lambda) at point x
     */
    public static double exponential(double x, double lambda) {
        return Math.log(lambda) - lambda * x;
    }
    
    /**
     * Density function for Exponential distribution
     * 
     * @param x Array of points at which density is to be computed
     * @param lambda Mean of the exponential distribution
     * @return Array of log-densities of exp(lambda) at points x[0], x[1], ...
     */
    public static double[] exponential(double[] x, double lambda) {
        double[] v = new double[x.length];
        double u = Math.log(lambda);
        for (int j=0; j<v.length; j++) {
            v[j] = u - lambda * x[j];
        }
        return v;
    }
    
    /**
     * Density function for Beta distribution
     * 
     * @param x Point at which density is to be computed
     * @param alpha Shape parameter
     * @param beta Shape parameter
     * @return Log-density of Beta(alpha,beta) at x
     */
    public static double beta(double x, double alpha, double beta) {
        return (alpha-1) * Math.log(x) + (beta-1) * Math.log(1.0-x) +
                lngamma(alpha+beta) - lngamma(alpha) - lngamma(beta);
    }
    
     /**
     * Density function for Beta distribution
     * 
     * @param x Array of points at which density is to be computed
     * @param alpha Shape parameter
     * @param beta Shape parameter
     * @return Array of log-densities of Beta(alpha,beta) at points x[0], x[1], ...
     */
    public static double[] beta(double[] x, double alpha, double beta) {
        double[] v = new double[x.length];
        double u = lngamma(alpha+beta) - lngamma(alpha) - lngamma(beta);
        for(int j=0; j<v.length; j++){
            v[j] = (alpha-1) * Math.log(x[j]) + (beta-1) * Math.log(1.0-x[j]) + u;
        }
        return v;
    }
    
    /**
     * Density function for Gamma distribution
     * 
     * @param x Point at which density is to be computed
     * @param alpha Shape parameter of Gamma distribution
     * @param beta Rate parameter of Gamma distribution
     * @return Log-density of Gamma(alpha,beta) at x
     */
    public static double gamma(double x, double alpha, double beta) {
        return alpha * Math.log(beta) - lngamma(alpha) + 
                (alpha-1) * Math.log(x) - beta * x;
    }
    
    /**
     * Density function for Gamma distribution
     * 
     * @param x Array of points at which density is to be computed
     * @param alpha Shape parameter of Gamma distribution
     * @param beta Rate parameter of Gamma distribution
     * @return Array of log-density of Gamma(alpha,beta) at x[0], x[1], ...
     */        
    public static double[] gamma(double[] x, double alpha, double beta) {
        double w = alpha * Math.log(beta) - lngamma(alpha);
        return Arrays.stream(x)
                     .map(u -> (alpha-1) * Math.log(u) - beta * u + w)
                     .toArray();
    }
    
    /**
     * Density function for non-standardized Student-t distribution
     * 
     * @param x Point at which density is to be computed
     * @param mu Location parameter of distribution
     * @param sigma Scale parameter of distribution
     * @param k Degrees of freedom
     * @return Log-density of t(mu,sigma,k) at x
     */
    public static double studentt(double x, double mu, double sigma, double k) {
        double delta = x - mu;
        double ratio = delta / sigma;
        double k_2 = k / 2.0;
        return lngamma(k_2 + 0.5) - lngamma(k_2) - 0.5 * Math.log(k * Math.PI) - 
                Math.log(sigma) - (k_2 + 0.5) * Math.log(1.0 + ratio * ratio / k); 
    }
 
    /**
     * Density function for non-standardized Student-t distribution
     * 
     * @param x Array of points at which density is to be computed
     * @param mu Location parameter of distribution
     * @param sigma Scale parameter of distribution
     * @param k Degrees of freedom
     * @return Array of log-density of t(mu,sigma,k) at x[0], x[1], ...
     */    
    public static double[] studentt(double[] x, double mu, double sigma, double k) {
        double m = k / 2.0 + 0.5;
        double u = lngamma(m) - lngamma(k / 2.0) - 0.5 * Math.log(k * Math.PI) - Math.log(sigma);
        double[] v = new double[x.length];
        for (int j=0; j<v.length; j++) {
            double delta = x[j] - mu;
            double ratio = delta / sigma;
            v[j] = u + m * Math.log(1.0 + ratio * ratio / k);
        }
        return v;
    }
    
    /**
     * Density function for Log-normal distribution
     * @param x Point at which density is to be computed
     * @param mu Mean of the natural log of the random variable
     * @param sigma Standard deviation of the natural logarithm of the variable
     * @return Log-density of lnN(mu,sigma) at x
     */
    public static double lognormal(double x, double mu, double sigma) {
        double lnx = Math.log(x);
        double u = -Math.log(sigma) - logRoot2pi;
        double z = (lnx - mu) / sigma;
        return u - lnx - z * z / 2;
    }
    
    /**
     * Density function for Log-normal distribution
     * @param x Array of points at which density is to be computed
     * @param mu Mean of the natural log of the random variable
     * @param sigma Standard deviation of the natural logarithm of the variable
     * @return Array of log-densities of lnN(mu,sigma) at points x[0], x[1], ...
     */
    public static double[] lognormal(double x[], double mu, double sigma) {
        double[] v = new double[x.length];
        double u = -Math.log(sigma) - logRoot2pi;
        for(int j=0; j<v.length; j++){
            double lnx = Math.log(x[j]);
            double z = (lnx - mu) / sigma;
            v[j] = u - lnx - z * z / 2;
        }
        return v;
    }
    
    /**
     * 
     * 
     * @param k Number of successes
     * @param n Number of trials
     * @param p Probability of success per trial
     * @return Log-mass of binomial distribution B(n,p) for k successes
     */
    public static double binomial(int k, int n, double p) {
        return ln_nCk(n,k) + k * Math.log(p) + (n-k) * Math.log(1.0-p);
    }

    /**
     * Mass function for binomial distribution
     * 
     * @param k Array of success counts
     * @param n Number of trials
     * @param p Probability of success per trial
     * @return Array of log-mass of B(n,p) for k[0], k[1], ...
     */
    public static double[] binomial(int[] k, int n, double p) {
        double log_p = Math.log(p);
        double log_1mp = Math.log(1.0-p);
        return Arrays.stream(k)
                     .mapToDouble(u -> ln_nCk(n,u) + u * log_p + (n-u) * log_1mp)
                     .toArray();
    }
    
    
    /**
     * Mass function for Bernoulli distribution
     * 
     * @param k {0, 1} failure/success indicator
     * @param p probability of success
     * @return Log-mass of Bern(p) for indicator k
     */
    public static double bernoulli(int k, double p) {
        return k == 1 ? Math.log(p) : Math.log(1.0-p);
    }

    /**
     * Mass function for Bernoulli distribution
     * 
     * @param k Array of {0, 1} failure/success indicators
     * @param p probability of success
     * @return  Array of log-mass of Bern(p) for each indicator k[0], k[1], ...
     */
    public static double[] bernoulli(int[] k, double p) {
        double[] logp = { Math.log(1.0-p), Math.log(p) };
        return Arrays.stream(k).mapToDouble(x -> logp[x]).toArray();
    }
    
    /**
     * Density function for Weibull distribution
     * 
     * @param x Point at which density is to be computed
     * @param k shape parameter
     * @param lambda scale parameter
     * @return Log-density of WEB(k, lambda) at x 
     */
    public static double weibull(double x, double k, double lambda) {
        if (x >= 0) {
            double u = x / lambda;
            return Math.log(k / lambda) + (k-1) * Math.log(u) - Math.pow(u, k);
        } else {
            return Double.NEGATIVE_INFINITY;
        }
    }
 
    /**
     * Density function for Weibull distribution
     * 
     * @param x Array of points at which density is to be computed
     * @param k shape parameter
     * @param lambda scale parameter
     * @return Array of log-density of WEB(k, lambda) at x[0], x[1], ... 
     */    
    public static double[] weibull(double[] x, double k, double lambda) {
        double[] v = new double[x.length];
        double w = Math.log(k / lambda);
        for (int j=0; j<x.length; j++) {
            if (x[j] >= 0) {
                double u = x[j] / lambda;
                v[j] = w + (k-1) * Math.log(u) - Math.pow(u, k);
            } else {
                v[j] = Double.NEGATIVE_INFINITY;
            }
        }
        return v;
    }
    
    /**
     * Density function for Poisson distribution
     * 
     * @param k Observed number of events in fixed time interval
     * @param lambda Expected number of events in fixed time interval
     * @return log-density of Poisson(lambda) at k
     */
    public static double poisson(int k, double lambda) {
        return k * Math.log(lambda) - lambda - lngamma(k + 1.0);
    }
    
    /**
     * Density function for Poisson distribution
     * 
     * @param k Array of observed event counts in fixed time interval
     * @param lambda Expected number of events in fixed time interval
     * @return Array of log-density of Poisson(lambda) at k[0], k[1], ...
     */    
    public static double[] poisson(int[] k, double lambda) {
        double w = Math.log(lambda);
        return Arrays.stream(k)
                     .mapToDouble(x -> x * w - lambda - lngamma(x + 1.0))
                     .toArray();
    }
    
    // approximation of log(gamma(z))
    // approximation for z >= 1 adapted from: 
    //   https://en.wikipedia.org/wiki/Gamma_function#The_log-gamma_function
    // for small z, defers to iterative method
    private static double lngamma(double z) {
        if (z < 1.0) // defer to slower method for small z
            return lngamma2(z, 1.e-7);
        double zsq = z*z;
        double u = (z - 0.5) * Math.log(z) - z + logRoot2pi + 1/(12*z);
        z *= zsq;
        u -= 1 / (360 * z);
        z *= zsq;
        u += 1 / (1260 * z);
        return  u;
    }
    
    // log(gamma(z))
    // adapted from:
    //   http://keisan.casio.com/exec/system/1180573442
    private static double lngamma2(double z, double prec) {
        double sum=0.0;
        
        double part;
        double k = 0.0;
        do {
            k += 1.0;
            double u = z / k;
            part = u - Math.log(1.0+u);
            sum += part;
        } while (part > prec);
        
        return sum - Math.log(z) - z * emc;
    }  
    
    // approximate log of binomial coefficient (n,k)
    // this approximation results from plugging Stirling's approximation
    // into definition of binomial coefficient
    private static double ln_nCk(double n, double k) {
        double d = n - k;
        return (n + 0.5) * Math.log(n) -
               (k + 0.5) * Math.log(k) -
               (d + 0.5) * Math.log(d) -
               logRoot2pi;
    }
}

