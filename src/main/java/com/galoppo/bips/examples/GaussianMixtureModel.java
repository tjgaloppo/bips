/*
 * Copyright © 2016, Travis Galoppo <tjg2107@columbia.edu>
 *
 * Permission to use, copy, modify, and/or distribute this software for any 
 * purpose with or without fee is hereby granted, provided that the above 
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES 
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY 
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF 
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN 
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
package com.galoppo.bips.examples;

import static com.galoppo.bips.Densities.*;
import com.galoppo.bips.Density;
import com.galoppo.bips.Sampler;
import com.galoppo.bips.graph.SummaryPlot;
import com.galoppo.bips.utils.Summary;
import com.galoppo.bips.utils.Utils;
import java.io.File;
import java.util.List;
import java.util.function.BiFunction;
import java.util.stream.Stream;

/**
 *
 * @author tgaloppo
 */
public class GaussianMixtureModel extends AbstractExample {
    
    public static void main(String[] args) throws Exception {
        if (args.length < 1) {
            System.out.println("Please provide an input file");
            return;
        }
        
        double[][] data = Utils.readData(new File(args[0]), false);
        
        // We assume the model has a mixuture of 2 2-d Gaussians; each will
        // need a center, and variance/covariance params...
        Density mu11_prior  = (x) -> gaussian(x[0], 0, 10);
        Density mu12_prior  = (x) -> gaussian(x[1], 0, 10);
        Density var11_prior = (x) -> gamma(x[2], 4, 4);
        Density var12_prior = (x) -> gamma(x[3], 4, 4);
        Density cov1_prior  = (x) -> uniform(x[4], -Math.sqrt(x[2]*x[3]), Math.sqrt(x[2]*x[3]));
        
        Density mu21_prior  = (x) -> gaussian(x[5], 0, 10);
        Density mu22_prior  = (x) -> gaussian(x[6], 0, 10);
        Density var21_prior = (x) -> gamma(x[7], 4, 4);
        Density var22_prior = (x) -> gamma(x[8], 4, 4);
        Density cov2_prior  = (x) -> uniform(x[9], -Math.sqrt(x[7]*x[8]), Math.sqrt(x[7]*x[8]));

        // likelihood function
        BiFunction<double[], double[], Double> logSum = (a,b) -> Stream.iterate(0, (n)->n+1).limit(a.length).mapToDouble(j -> Math.log(a[j]+b[j])).sum(); 
        Density likelihood = (x) -> logSum.apply(mvnormal2d(data, x[0], x[1], x[2], x[3], x[4]), mvnormal2d(data, x[5], x[6], x[7], x[8], x[9]));
        
        Density[] model = {
            (x) -> mu11_prior.p(x) + likelihood.p(x),
            (x) -> mu12_prior.p(x) + likelihood.p(x),
            (x) -> var11_prior.p(x) + likelihood.p(x),
            (x) -> var12_prior.p(x) + likelihood.p(x),
            (x) -> cov1_prior.p(x) + likelihood.p(x),
            
            (x) -> mu21_prior.p(x) + likelihood.p(x),
            (x) -> mu22_prior.p(x) + likelihood.p(x),
            (x) -> var21_prior.p(x) + likelihood.p(x),
            (x) -> var22_prior.p(x) + likelihood.p(x),
            (x) -> cov2_prior.p(x) + likelihood.p(x)
        };
        
        double[] xinit = { 0.0, 0.0, 5.0, 5.0, 0.0,
                           0.0, 0.0, 5.0, 5.0, 0.0 };
        
        // Sample the (proportional) posterior, and thin the results
        List<double[]> samps = new Sampler(model, xinit)
                            .setBurnLength(getBurnCount())
                            .setThinningFactor(getThinStep())
                            .sample(getSampleCount());

        String[] labels = {"mu_11", "mu_12", "var_11", "var_12", "cov1","mu_21", "mu_22", "var_21", "var_22", "cov2"};
        System.out.println(new Summary(samps, labels).toString());
        new SummaryPlot(samps, 15, labels).setVisible(true);        
    }
    
    /**
     * PDF of 2-dimensional Gaussian
     * 
     * Computes density of 2D Gaussian distribution with center (mu1, mu2) and
     * covariance matrix
     * 
     * var1 cov12
     * cov12 var2
     * 
     * at point (x1, x2).
     * 
     * Note this is not log-density
     * 
     * @param x NxM matrix of N M-dimensional points at which to evaluate density
     * @param mu1 Mean of first dimension
     * @param mu2 Mean of second dimension
     * @param var1 Variance of first dimension
     * @param var2 Variance of second dimension
     * @param cov12 Covariance of first and second dimensions
     * @return Array of probability densities at each x[] 
     */    
    public static double[] mvnormal2d(double[][] x, double mu1, double mu2, double var1, double var2, double cov12) {
        double[] p = new double[x.length];
        double det = var1 * var2 - cov12 * cov12;
        double w = Math.pow(det, -0.5) / (2.0 * Math.PI);
        for (int j=0; j<p.length; j++) {
            double delta1 = x[j][0] - mu1;
            double delta2 = x[j][1] - mu2;
            double u = delta1 * var2 - delta2 * cov12;
            double v = delta2 * var1 - delta1 * cov12;
            p[j] = Math.exp(-0.5 * (delta1 * u + delta2 * v) / det) * w;
        }
        return p;
    }
}
