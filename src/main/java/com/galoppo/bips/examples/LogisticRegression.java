/*
 * Copyright © 2016, Travis Galoppo <tjg2107@columbia.edu>
 *
 * Permission to use, copy, modify, and/or distribute this software for any 
 * purpose with or without fee is hereby granted, provided that the above 
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES 
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY 
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF 
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN 
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
package com.galoppo.bips.examples;

import static com.galoppo.bips.Densities.gaussian;
import com.galoppo.bips.Density;
import com.galoppo.bips.MultiChainSampler;
import com.galoppo.bips.Sampler;
import static com.galoppo.bips.examples.AbstractExample.getBurnCount;
import static com.galoppo.bips.examples.AbstractExample.getSampleCount;
import static com.galoppo.bips.examples.AbstractExample.getThinStep;
import com.galoppo.bips.graph.SummaryPlot;
import com.galoppo.bips.utils.Summary;
import com.galoppo.bips.utils.Utils;
import java.io.File;
import java.util.List;
import java.util.stream.Stream;

/**
 *
 * @author ckogan
 */
public class LogisticRegression {
    public static void main(String[] args) throws Exception{
        double[][] x = Utils.readData(new File(args[0]), false);
//        double[][] x = Utils.readData(new File("data/logistic.csv"), false);
        Density beta_prior = (u) -> gaussian(u[0], 0, 1);
        Density likelihood = (u) -> Stream.iterate(0, (n) -> (n+1))
                                          .limit(x.length)
                                          .mapToDouble((i) -> LogisticLogLikelihood(x[i][0], x[i][1], u[0]))
                                          .sum();
        Density[] model = {
            (u) -> likelihood.p(u) + beta_prior.p(u)
        };     
        Sampler sampler1 = new Sampler(model)
                               .setBurnLength(getBurnCount())
                               .setThinningFactor(getThinStep());
        
        List<double[]> samps = sampler1.sample(getSampleCount());
        samps.addAll(sampler1.sample(getSampleCount()));                       
        System.out.printf("The chain has a total of %d samples\n", samps.size());
        
        System.out.println(new Summary(samps, new String[]{"beta"}).toString());
        new SummaryPlot(samps, 15, new String[]{"beta"}).setVisible(true);
        
        // Multiple chains      
        List<List<double[]>> msamps = new MultiChainSampler(model)
                                .setInitialConditions(new double[][] {{-0.5},{0.5}})
                                .setNumberOfChains(2)
                                .setBurnLength(getBurnCount())
                                .setThinningFactor(getThinStep())
                                .setNumberOfSamples(getSampleCount())
                                .sample();
        
        int chainIndex = 0;
        String[] labels = { "beta" };
        for (List<double[]> chain : msamps) {
            System.out.printf(String.format("Chain %d:\n", ++chainIndex));
            System.out.println(new Summary(chain, labels).toString());
            new SummaryPlot(chain, 15, labels).setVisible(true);
        }
    }
    
//    public static double LogisticLogLikelihood(double y, double x, double beta){
//        double exponent = Math.exp(-x * beta);
//        double logistic = 1 / (1 + exponent);
//        return y * Math.log(logistic) + (1 - y) * Math.log(1 - logistic);
//    }
    
    // equal to above (commented) method, refactored to require single logarithm
    public static double LogisticLogLikelihood(double y, double x, double beta) {
        double u = -x * beta;
        return (1 - y) * u - Math.log(1.0 + Math.exp(u));
    }
}
