/*
 * Copyright © 2016, Travis Galoppo <tjg2107@columbia.edu>
 *
 * Permission to use, copy, modify, and/or distribute this software for any 
 * purpose with or without fee is hereby granted, provided that the above 
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES 
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY 
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF 
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN 
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
package com.galoppo.bips.examples;

import com.galoppo.bips.Sampler;
import static com.galoppo.bips.Densities.*;
import com.galoppo.bips.Density;
import com.galoppo.bips.graph.SummaryPlot;
import com.galoppo.bips.utils.Summary;
import com.galoppo.bips.utils.Utils;
import java.io.File;
import java.util.List;
import java.util.stream.Stream;

/**
 * 
 * @author tgaloppo
 */
public class LinearRegression extends AbstractExample {       
    public static void main(String[] args) throws Exception {
        if (args.length < 1) {
            System.out.println("Please provide an input file");
            return;
        }
        double[][] x = Utils.readData(new File(args[0]), false);
        
        // create priors on random vars
        Density beta0_prior   = (u) -> gaussian(u[0], 4., 2.); // prior on beta0
        Density beta1_prior   = (u) -> gaussian(u[1], 4., 2.); // prior on beta1
        Density beta2_prior   = (u) -> gaussian(u[2], 4., 2.); // prior on beta2
        Density err_prior     = (u) -> gamma(u[3], 4, 4);      // prior on error
        // and the likelihood function
        Density likelihood    = (u) -> Stream.iterate(0, (n)->n+1)
                                             .limit(x.length)
                                             .mapToDouble(i -> gaussian(x[i][0], u[0]*x[i][1]+u[1]*x[i][2]+u[2]*x[i][3], u[3]))
                                             .sum();
        
        Density[] model = {
            (u) -> beta0_prior.p(u) + likelihood.p(u),
            (u) -> beta1_prior.p(u) + likelihood.p(u),
            (u) -> beta2_prior.p(u) + likelihood.p(u),
            (u) -> err_prior.p(u) + likelihood.p(u),
        };
        
        List<double[]> samps = new Sampler(model)
                               .setBurnLength(getBurnCount())
                               .setThinningFactor(getThinStep())
                               .sample(getSampleCount());

        System.out.println(new Summary(samps, new String[]{"beta0","beta1","beta2","epsilon"}).toString());
        new SummaryPlot(samps, 15, new String[]{"beta0","beta1","beta2","epsilon"}).setVisible(true);
    }
}
