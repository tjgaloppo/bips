/*
 * Copyright © 2016, Travis Galoppo <tjg2107@columbia.edu>
 *
 * Permission to use, copy, modify, and/or distribute this software for any 
 * purpose with or without fee is hereby granted, provided that the above 
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES 
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY 
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF 
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN 
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
package com.galoppo.bips.examples;

import static com.galoppo.bips.Densities.*;
import com.galoppo.bips.Density;
import com.galoppo.bips.Sampler;
import com.galoppo.bips.graph.SummaryPlot;
import com.galoppo.bips.utils.Summary;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author tgaloppo
 */
public class ConjugatePrior extends AbstractExample {
    // x ~ N(3,2)
    final static double[] u = { 2.9597393, 2.4353282, 2.0652539, 3.2395144, 5.4600048, 
                                2.6722985, 2.3950332, 2.2766307, 4.5472265, 0.4123851 };
    
    public static void main(String[] args) {
        double mu_prior = 0.0;
        double std_prior = 3.0;
        double sigma_x = 2.0;        
        
        // Explicit posterior, Gaussian prior on mean of Gaussian distribution
        // (we assume to know the standard deviation of the distribution)
        Density[] model1 = {
            x -> gaussian(x[0], mu_prior, std_prior) + Arrays.stream(gaussian(u, x[0], sigma_x)).sum()
        };
        
        long t0 = System.currentTimeMillis();
        List<double[]> samples = new Sampler(model1)
                                    .setBurnLength(getBurnCount())
                                    .setThinningFactor(getThinStep())
                                    .sample(getSampleCount());
        long t1 = System.currentTimeMillis();
        
        System.out.printf("time = %d ms\n", t1-t0);
        System.out.println(new Summary(samples).toString());
        System.out.println();
        new SummaryPlot(samples, 15, new String[]{"mu"}).setVisible(true);
        
        // The resulting posterior is itself Gaussian, and we can make the
        // sampler's work easier by computing the parameters directly
        double n = u.length;
        double sum_x = Arrays.stream(u).sum();
               
        double mu_post = (mu_prior * sigma_x * sigma_x + sum_x * std_prior * std_prior) / (sigma_x * sigma_x + n * std_prior * std_prior);
        double std_post = Math.sqrt(sigma_x*sigma_x*std_prior*std_prior/(sigma_x*sigma_x+n*std_prior*std_prior));
        
        System.out.printf("mu_post=%f, sigma_post=%f\n", mu_post, std_post);
        
        Density[] model2 = {
            x -> gaussian(x[0], mu_post, std_post)
        };
    
        t0 = System.currentTimeMillis();
        samples = new Sampler(model2)
                        .setBurnLength(getBurnCount())
                        .setThinningFactor(getThinStep())
                        .sample(getSampleCount()); 
        t1 = System.currentTimeMillis();
        
        System.out.printf("time = %d ms\n", t1-t0);
        System.out.println(new Summary(samples).toString());
        System.out.println();   
        new SummaryPlot(samples, 15, new String[]{"mu"}).setVisible(true);
    }
}
