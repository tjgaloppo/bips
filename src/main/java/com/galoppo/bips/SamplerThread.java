/*
 * Copyright © 2016, Travis Galoppo <tjg2107@columbia.edu>
 *
 * Permission to use, copy, modify, and/or distribute this software for any 
 * purpose with or without fee is hereby granted, provided that the above 
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES 
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY 
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF 
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN 
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
package com.galoppo.bips;

import java.util.List;

public class SamplerThread extends Sampler implements Runnable {
    private int n;
    private List<double[]> samples;
    public SamplerThread(Density[] f) {
        super(f);
    }
    
    public SamplerThread(Density[] f, double[] initialConditions) {
        super(f, initialConditions);
    }
    
    public SamplerThread setSamplesLength(int n){
        this.n = n;
        return this;
    }
    
    public List<double[]> getSamples(){
        return samples;
    }
    
    @Override
    public void run() {
       samples = this.sample(n);
    }
}
